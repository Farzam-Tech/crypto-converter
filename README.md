# Crypto Converter

In this project I have created a bot using AWS-LEX and AWS-Lambda to allow customers to convert from dollars to one of these three cryptocurrencies: Bitcoin, Ethereum, and Ripple.

# Instructions

### Initial Crypto Converter Configuration(AWS Lex)
In this section, I have created the Crypto-Converter bot and add an intent with its corresponding slots.

1) Log-in to the AWS Management Console using your administrator user, navigate to the Amazon Lex console and create a new custom slot in the convertCAD intent of the Crypto_Converter bot as follows:

- Slot type name: Cryptocurrency
- Description: Available cryptocurrencies to convert.
- Slot Resolution: Choose "Restrict to Slot values and Synonyms".
- Value: Add three values and synonyms, one per each of the following cryptocurrencies.


2) Create the intent, and configure some sample utterances as follows:

- I want to invest in cryptocurrencies
- I want to convert dollars to a cryptocurrency
- I want to buy {crypto}
- I want to convert CAD to {crypto}
- I want to convert dollars to {crypto}
- I want to convert {cadAmount} dollars to {crypto}

3) Build the bot and test it in the "Test bot" window. 

## Enhance the Crypto Converter with an Amazon Lambda Function

In this section I have Created a Amazon Lambda function. In the Amazon Lambda function you can find a helper function called get_cryptoprice(crypto) that will receive as parameter the name of the cryptocurrency selected by the user. This function should return the price of the chosen cryptocurrency in Canadian Dollars. To retrieve the current price of Ethereum and Ripple, I have used the following endpoints from the alternative.me Crypto API.

- Ethereum: https://api.alternative.me/v2/ticker/Ethereum/?convert=CAD
- Ripple: https://api.alternative.me/v2/ticker/Ripple/?convert=CAD



# Build the bot and test it in the "Test bot" window. 

![Extended Crypto Converted demo](Images/crypto_converter_extended.gif)

